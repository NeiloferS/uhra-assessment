# UHRA Team Selection Assessment

Welcome to the UH Racing Autonomous (UHRA) Team Selection Assessment! This repository contains the assessment materials and instructions for candidates applying to join our UHRA team. We are excited to have you here and are looking forward to assessing your technical skills and potential.

## Assessment Objectives

This assessment is primarily designed to evaluate candidates in the following areas:

1. **Technical Skills**: Assess your programming skills and ability to work on AI and machine learning projects.

2. **Logical Reasoning**: Evaluate your problem-solving and algorithmic thinking abilities.

3. **Professionalism**: Measure your professionalism, teamwork, and communication skills.

## Getting Started

Please follow these steps to participate in the assessment:

1. **Fork this Repository**: Click the "Fork" button in the upper-right corner of this repository to create a copy under your own GitHub account.

2. **Programming Assessment**:
   - Inside the repository, you will find instructions for coding challenges and algorithmic challenges. Follow the instructions in the file.
   - Complete the coding and algorithmic challenges and make sure your submission meets the specified success criteria.
   - Submit your solution by creating a Pull Request (PR) to the `main` branch of this repository.

3. **Logical Reasoning Assessment (Mentimeter)**:
   - Inside the repository, you will find instructions for logical reasoning challenges. Follow the instructions in the file.
   - Complete the logical reasoning challenges and make sure your submission meets the specified success criteria.
   - Submit your solution by creating a Pull Request (PR) to the `main` branch of this repository.

4. **Professionalism Assessment**:
   - During the interview or group discussion, we will evaluate your professionalism, teamwork, and communication skills. Be prepared to discuss your experiences and how you approach teamwork and challenges.

## Evaluation and Scoring

Candidates will be evaluated based on the following criteria:

- Programming Assessment: Code correctness, efficiency, and documentation.
- Logical Reasoning Assessment: Correctness and logical thinking.
- Professionalism Assessment: Behavior, teamwork, and effective communication during the interview or group discussion.

## Candidate Feedback

After the assessment process, we will provide constructive feedback to all candidates, highlighting your strengths and areas for improvement.

## Selection Process

Candidates with the highest overall scores will be selected for further consideration in our team. We will notify selected candidates about the next steps in the process.

## Continuous Improvement

We value feedback and continuously strive to improve our assessment process. After the assessment, we encourage candidates to share their thoughts and suggestions with us to enhance our future selections.

If you have any questions or encounter any issues during the assessment, please reach out to our team for assistance.

Thank you for your interest in joining the UH Racing Autonomous Team!
