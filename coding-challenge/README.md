# UHRA Coding Challenges

Welcome to the UHRA Coding Challenges Asssessment! Here, you'll find a set of coding challenges designed to assess your skills in programming related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitHub account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/coding-challenge.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `challenge-1-John-Simth.py`, `challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your submission will be reviewed**, and feedback may be provided.

## Challenge Descriptions

### Beginner Level Challenges

1. **Challenge 1: Calculate Factorial**: Write a Python function calculate_factorial(n) that calculates the factorial of a non-negative integer n. The factorial of a number is the product of all positive integers up to and including n.

2. **Challenge 2: Sum of Even Numbers**: Write a Python program that calculates the sum of all even numbers from 1 to a given positive integer n.

### Intermediate Level Challenges

3. **Challenge 3: Find Common Elements**: Write a Python function find_common_elements(list1, list2) that takes two lists as input and returns a new list containing the elements that are common to both input lists, with no duplicates.

4. **Challenge 4: Word Frequency Counter**: Write a Python program that reads a text file and counts the frequency of each word in the file. Ignore punctuation and consider words in a case-insensitive manner.

### Advanced Level Challenge

5. **Challenge 5: Longest Common Subsequence**: Implement a Python function longest_common_subsequence(str1, str2) that finds the longest common subsequence of two input strings str1 and str2. A subsequence is a sequence of characters that appears in the same relative order but not necessarily contiguous.

## Have Fun and Learn!

These challenges offer an opportunity to test your programming skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck!
