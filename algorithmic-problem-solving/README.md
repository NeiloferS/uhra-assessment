# UHRA Algorithmic Challenges

Welcome to the UHRA Algorithmic Challenges Asssessment! Here, you'll find a set of algorithmic challenges designed to assess your skills in programming and problem solving related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitHub account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/algorithmic-problem-solving.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `challenge-1-John-Simth.py`, `challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your submission will be reviewed**, and feedback may be provided.

## Challenge Descriptions

### Beginner Level Challenges

1. **Challenge 1: Sorting Algorithm**: Implement the bubble sort algorithm to sort a small list of integers. Provide the sorted list and the steps you took to sort it.

2. **Challenge 2: Graph Algorithm**: Write pseudocode or provide a step-by-step description of how you would perform a depth-first search (DFS) on an undirected graph, starting from a given node. Describe the order in which nodes are visited.

### Intermediate Level Challenges

3. **Challenge 3: Dynamic Programming**: You are given a set of positive integers. Write a Python program to find the length of the longest increasing subsequence in the set.

4. **Challenge 4: Search Algorithm**: You are given a sorted array of integers and a target value. Write a Python program to search for the target value in the array using binary search.

### Advanced Level Challenge

5. **Challenge 5: Complex Graph Algorithm**: You are given a weighted, directed graph with nodes and edges. Write pseudocode or provide a step-by-step description of how you would find the shortest path between two given nodes in the graph using Dijkstra's algorithm.

## Have Fun and Learn!

These challenges offer an opportunity to test your problem-solving skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck!
