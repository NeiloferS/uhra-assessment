# UHRA Logical Reasoning Challenges

Welcome to the UHRA Logical Reasoning Challenges Asssessment! Here, you'll find a set of logocal reasoning challenges designed to assess your critical thinking and analysing skills related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitHub account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/logical-reasoning.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `challenge-1-John-Simth.py`, `challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your submission will be reviewed**, and feedback may be provided.

## Challenge Descriptions

### Beginner-Level Challenges

1. **Challenge 1: Solve the following logical reasoning puzzle:**

Three friends are captured by an evil sorcerer. The sorcerer decides to have some fun with them. 

He puts them in a line, one behind the other, so that the person at the back can see the two people in front of them, the middle person can see the one in front of them, and the person in front can't see anyone. 
He then places a red or blue hat on each person's head. The sorcerer randomly selects the hat colors, and there is at least one hat of each color.

The sorcerer asks the friends, starting from the back of the line, to guess the color of their own hat. If they guess correctly, they will be set free; if not, they will be imprisoned forever.

The friends cannot communicate with each other, but they can hear each other's guesses. The friends know this rule and are allowed to discuss a strategy before they start guessing. What strategy can they use to maximize their chances of being set free?


2. **Challenge 2: Solve the following logical reasoning puzzle:**

A king suspects that someone in his court is trying to poison him. He has four loyal servants who each serve him wine. The servants are Walter, Alice, Tom, and Lucy. The king decides to test them. He gives each servant a glass of wine and watches them closely.

Walter poured the wine into a jug, and then he poured the jug into the king's glass.

Alice poured the wine directly from the bottle into the king's glass.

Tom poured the wine into a cup, and then he poured the cup into the king's glass.

Lucy poured the wine directly from the bottle into the king's glass.

Shortly after drinking the wine, the king falls ill, confirming his suspicions. Which servant is the poisoner?


### Intermediate-Level Challenges

3. **Challenge 3: Solve the following logical reasoning puzzle:**

Five people of different nationalities live in a row of houses. Each person drinks a different beverage, smokes a different brand of cigar, and keeps a different pet. The question is: Who has a fish?

Hints:

The Brit lives in the red house.

The Swede keeps dogs as pets.

The Dane drinks tea.

The green house is on the left of the white house.

The person who smokes Pall Mall rears birds.

The owner of the yellow house smokes Dunhill.

The man living in the house right in the center drinks milk.

The Norwegian lives in the first house.

The man who smokes Blend lives next to the one who keeps cats.

The man who keeps the horse lives next to the man who smokes Dunhill.

The owner who smokes Blue Master drinks beer.

The German smokes Prince.

The Norwegian lives next to the blue house.

The man who smokes Blend has a neighbor who drinks water.


4. **Challenge 4: Solve the following logical reasoning puzzle:**

You are in a room with two doors. One door leads to certain death, and the other door leads to freedom. There are two guards, one in front of each door. One guard always tells the truth, and the other always lies. You don't know which guard is which, and you don't know which door leads to freedom.

What questions can you ask to figure out the tribe of each person?


### Advanced Level Challenge

5. **Challenge 5: The Mystery of the Locked Room:**

Late one evening, Mr. Smith was found dead in his study. The room was locked from the inside, and there were no signs of forced entry. The only people in the house at the time of the incident were Mr. Smith, his wife, his butler, and his maid.

The police questioned the three remaining individuals and received the following statements:

Mrs. Smith: "I was in the living room watching television when I heard a gunshot from my husband's study. I rushed to the door, but it was locked. I couldn't have killed him; I was nowhere near his study."

The butler: "I was in the kitchen preparing dinner. I heard the gunshot too, but I couldn't have done it. My fingerprints are not on the murder weapon."

The maid: "I was in the laundry room, doing the laundry. I didn't hear the gunshot, and I had no reason to harm Mr. Smith."

The police found a pistol on Mr. Smith's desk, which had one fired bullet, but there were no fingerprints on it. There were no windows in the study, and the door was locked from the inside.

Who do you think is the most likely suspect, and how do you think Mr. Smith was killed?


## Have Fun and Learn!

These challenges offer an opportunity to test your critical thinking and analysing skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck! And remember You're Kenough!
